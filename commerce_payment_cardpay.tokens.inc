<?php

/**
 * Implements hook_token_info().
 */
function commerce_payment_cardpay_token_info()
{
    $invoice = array(
        'formated-invoice-number' => array(
            'name' => t('Invoice number (formated)'),
            'description' => t('Invoice number formated YYYYMM0000'),
            'type' => 'commerce-invoice',
        ),
    );

    return array(
        'tokens' => array('commerce-invoice' => $invoice),
    );
}

/**
 * Implements hook_tokens().
 */
function commerce_payment_cardpay_tokens($type, $tokens, array $data = array(), array $options = array()) {
    $replacements = array();
    if ($type == 'commerce-invoice' && !empty($data['commerce-invoice'])) {
        $invoice = $data['commerce-invoice'];

        foreach ($tokens as $name => $original) {
            switch ($name) {
                case 'formated-invoice-number':
                    $expl = explode('-', $invoice->invoice_number);
                    $replacements[$original] = sprintf('%s%02d%04d', $expl[0], $expl[1], $expl[2]);
                    break;
            }
        }
    }

    return $replacements;
}
