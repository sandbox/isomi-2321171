<?php
/*
	Copyright 2009 MONOGRAM Technologies

	This file is part of MONOGRAM EPayment libraries

	MONOGRAM EPayment libraries is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	MONOGRAM EPayment libraries is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with MONOGRAM EPayment libraries.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once dirname(__FILE__).'/EPaymentMessage.class.php';

abstract class EPaymentAesSignedMessage extends EPaymentMessage {
    public function computeSign($sharedSecret) {
        if (!$this->isValid)
            throw new Exception(__METHOD__.": Message was not validated.");

        try {
            $bytesHash = sha1($this->GetSignatureBase(), TRUE);
            $bytesHash = substr($bytesHash, 0, 16);
            //$sign = strtoupper(bin2hex($bytesSign));
            //$sign = aes256_cbc_encrypt($sharedSecret, $bytesSign,$iv);
//            $data = $this->GetSignatureBase();  
            $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB);
            $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
            $key = pack('H*', $sharedSecret);
            # show key size use either 16, 24 or 32 byte keys for AES-128, 192
            # and 256 respectively
//            echo bin2hex($bytesHash);                  
//            echo "\n";
            $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $bytesHash, MCRYPT_MODE_ECB, $iv);
//            echo(bin2hex($crypttext));
//            echo "\n";
//            echo strtoupper(bin2hex($crypttext));
//            echo "\n";
//            echo strlen($crypttext) . "\n";
        } catch (Exception $e) {
            return false;
        }
        return strtoupper(bin2hex($crypttext));
    }
}